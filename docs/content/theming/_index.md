---
title: Theming
weight: 80

---

For convenience, a symlink is available at the root of the project: `theme/`.

Parent theme
------------

Since HPFB WxT is built with Drupal, the presentation layer is handled with a "theme"; in this case, `hpfb_wxt_bootstrap` (a subtheme of [WxT Bootstrap](https://www.drupal.org/project/wxt_bootstrap)).


Styling
-------

To manage CSS efficiently, we use [Sass](https://sass-lang.com/). The first thing you'll need to do is install it, which we can do with [Drumkit](https://drumk.it):
```
make sass
```

To run Sass, you have to be in the theme directory:
```
cd theme/
sass sass:css --watch
```
