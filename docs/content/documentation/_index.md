---
title: Documentation
weight: 100

---

We maintain the HPFB WxT documentation site using [Hugo](http://www.gohugo.org/). To get started contributing to [this project](https://gitlab.com/hres/hpfb-wxt) fork it on Gitlab and install Hugo:

```console
$ git clone --recursive https://gitlab.com/hres/hpfb-wxt.git hpfb-wxt
$ cd hpfb-wxt
$ git remote add doc-sandbox https://gitlab.com/<username>/hpfb-wxt.git
$ make hugo    # this should install Hugo
$ hugo serve   # run the local server
```

Your local HPFB WxT docs site should now be available for browsing:
[http://localhost:1313/](http://localhost:1313/). When you find a typo, an
error, unclear or missing explanations or instructions, hit ctrl-c, to stop the
server, and start editing. Find the page you’d like to edit; everything is in
the `docs/content/` directory. Make your changes, commit and push them, and start a pull
request:

```console
$ git checkout -b fix_typo
$ vim docs/content/_index.md            # Add/edit/remove whatever you see fit. Be bold!
$ hugo serve                            # Go check your changes. We’ll wait...
$ git diff                              # Make sure there aren’t any unintended changes.
diff --git a/docs/content/_index.md b/docs/content/_index.md
...
$ git commit -am”Fixed typo.”           # Useful commit message are a good habit.
$ git push docs-sandbox fix_typo
```

Visit your fork on Gitlab and start a Merge Request.
