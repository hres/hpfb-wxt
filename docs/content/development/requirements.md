---
title: Requirements
weight: 10

---

### Unix

The Consensus Engineering team generally supports development on GNU/Linux
(mostly Debian/Ubuntu flavours) or OSX. If you're using another OS, consider
installing GNU/Linux in a dual-boot setup. Alternatively, you could develop in
a VM, either a cloud instance (e.g., DigitalOcean droplet) or locally using
[Vagrant](https://www.vagrantup.com/).


### Git & GitLab

We use [Git](https://git-scm.com) throughout our projects to manage our code, documentation, etc. Installing Git should be as simpole as:

```bash
apt install git   # For Debian

brew install git  # For OSX
```

Our code is hosted on [GitLab](https://gitlab.com/authenticity), so you'll need an [account](https://gitlab.com/users/sign_in), if you intend to [create a merge request](https://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html).


### Drumkit

[Drumkit](http://drumk.it) is a collection of [GNU Make](https://www.gnu.org/software/make/manual/make.html) scripts to simplify repetitive development tasks across projects. It (obviously) requires GNU Make, but otherwise comes bundled into projects as a Git submodule. So make sure to use `git clone --recursive` whenever you clone a project. If you forget to, you can always run `git submodule update --init` from within the project.

Installing GNU Make should be as easy as:

```bash
apt install make    # For Debian

brew install make   # For OSX
```

### Lando

We use [Lando](https://docs.lando.dev/) to help automate both local development and CI. If you don't already have it installed, [do so now](https://docs.lando.dev/basics/installation.html). 

### Composer

We also require [Composer](https://getcomposer.org/). If you don't already have it installed, Drumkit can do so for you very easily. From within the local git repo (see the next section), just run:

```bash
# This "sources" Drumkit's bootstrap script,
# altering you $PATH, and possibly running
# some other setup tasks. Use "source d" in zsh.
. d
make composer
```
