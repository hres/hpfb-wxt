---
title: Prepare a Release
weight: 10

---

## Introduction

Generally, a release will consist of a number of updates from the set of
tickets that have been closed and merged since the previous release. We use
GitLab's Merge Request workflow and git branches to track new development, and
our Continuous Integration (GitLab CI) validates the full Behat test suite runs
successfully as we merge in updates.

### Prepare the release

Preparing a release is primarily a matter of gathering (and possibly merging)
the various branches that were finished during the release cycle (sprint).
Ideally each of the branches has been merged along the way, and re-incorporated
into new work. If necessary, the release manager will need to resolve conflicts
and ensure all ready branches are merged into the `master` branch without
breaking the build.

### Tag the release

We use [Semantic Versioning](https://semver.org/), so release tags look like
`vM.m.p` where `M` is the Major version number, `m` is the minor version
number, and `p` is the patch level.

The purpose of tagging a release is to set a marker on the repository at a
point in time, to denote a "known state" of the code at that moment. This
serves to produce consistent, repeatable builds and deploys, as well as serving
as the basis for our [config update](../#configuration-updates) workflow.
