---
title: Config Management (CMI 2.0)
weight: 10
# TODO: Replace this page with documentation re. config_enforce
---


This page documents some background on the current state-of-the-art for Config Management in Drupal 8.

## Configuration Management 2.0

[CMI 2.0 project](https://www.drupal.org/project/ideas/issues/2957423) has a
comprehensive review of the key use-cases and contrib-space experiments in the
works. The comment thread on that issue is very active and the community is
working through approaches and experiments to improve the workflows around
config management in Drupal 8 generally.

The OpenSocial community has been tackling this problem in the
distribution/install profile space, and this [meta issue about how to handle
configuration deployment](https://www.drupal.org/project/social/issues/3092272)
is a helpful pointer to various relevant discussions and approaches or tools
for this problem.

* [Config Update](https://www.drupal.org/project/config_update) appears to be the de facto
  standard for handling the "update/revert" functionality for config
  entities in particular. It has grown out of a [core issue #1398040](https://www.drupal.org/project/ideas/issues/1398040) now 8yrs old,
  and a related [core issue #1497268](https://www.drupal.org/project/drupal/issues/1497268) that resolved
  to `config_update`. This effectively duplicates the equivalent functionality from [Features module](https://drupal.org/project/features)
  from Drupal 7, and in fact Features now [depends on Config Update](https://git.drupalcode.org/project/features/blob/8.x-3.8/features.info.yml#L8) for exactly this feature.
* https://www.drupal.org/project/update_helper appears to be the emerging
  standard for a tool that sits on top of `config_update` and produces "CUDs"
  or Config Update Definitions as well as programmatically writing
  `hook_update()` routines to effect the changes when the update is deployed.
* https://www.drupal.org/project/config_sync is another effort in a slightly
  different direction, that's built on top of several other experimental
  `config_*` projects in contrib:
  * [Config Distro](https://www.drupal.org/project/config_distro)
  * [Config Filter](https://www.drupal.org/project/config_filter)
  * [Config Merge](https://www.drupal.org/project/config_merge)
  * [Config Normalizer](https://www.drupal.org/project/config_normalizer)
  * [Config Provider](https://www.drupal.org/project/config_provider)
  * [Config Snapshot](https://www.drupal.org/project/config_snapshot)
  * and yes, the venerable [Config Update](https://www.drupal.org/project/config_update)


The README files for both config_update and update_helper are instructive as a framework for the configuration component of releasing updates:

* https://git.drupalcode.org/project/config_update/blob/8.x-1.x/config_update_ui/README.txt
* https://git.drupalcode.org/project/update_helper/blob/8.x-1.x/README.md
