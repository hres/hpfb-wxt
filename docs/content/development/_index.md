---
title: Development
weight: 5

---

This section covers hands-on example of development tasks, such as [updating the
codebase](#updating-the-codebase). See the [Architecture](/architeture) section
for a broader discussion of the applications underlying components, and how
they fit together.

Requirements
------------

**IMPORTANT** See the [Requirements](requirements) page and verify you have them before proceeding.

Getting the Code
----------------

```
git clone --recursive https://gitlab.com/hres/hpfb-wxt.git
cd hpfb-wxt
```

Installing the Drupal Site
--------------------------

```
. d            # Bootstrap Drumkit (use "source d" on non-Bash shells)
make start     # Start the Lando service containers
make build     # Build our codebase
make install   # Install the Drupal site
make dev       # (optional) Enable various development modules and components
```

Updating the Codebase
---------------------

Updating the codebase should be done locally using Lando, and the full test suite run successfully, before committing changes.

```
lando composer update
```

Updating Config and Content
---------------------------

In general, there are 3 main kinds of changes that need to be deployed as the application changes:

1. Configuration updates - anything the CMI system covers, represented by .yml files in the codebase.
2. Structural content updates - non-configuration items such as taxonomy terms, block content, or static pages that form the "structure" of the site or application even though they are technically content.
3. Code and/or DB changes - typically functional changes to a module's code are straightforward to deploy, but often involve changes to database schema such that a `hook_update()` routine is required.

### Configuration updates

At the moment, the [`update_helper`](https://drupal.org/project/update_helper)
is the primary tool with which to manage creating and deploying updates to a
site's configuration.

* Start a branch off of master, ideally at the last release tag.
* Do a `make install` to get the state of the config as of the latest release.
* Update any config required to effect the desired changes to the application (don't export at this point).
* Run something like `lando drupal gcu --module=cc_site_config --include-modules=cc_site_config` to generate an update:
  * the updated config is exported into its module's `config/install` directory
  * the selected module will have a `hook_update_N()` function written to its `.install` file
  * the selected module will have a new `config/update` YML file, representing the config change
* Commit those changes on your branch, eventually to be merged and tagged as part of a new release.

To test this:

* Do: `git checkout master` or the latest release tag to mirror production code/config.
* Do: `make install` again to reset your installed active config to the release tag version.
* Do: `git checkout my-new-branch` to get the `update_helper` commits.
* Run `lando drush updb` to run the `hook_update()` routines and effect the changes.
* Review the expected changes on your local site, confirm things deployed properly.

## Structural Content updates

Config management component covers some of the most common updates we see in an
application's lifecycle, and is least well-developed in Drupal 8, so will
likely require a majority of the effort here. The second piece, "structural
content" is another significant chunk that requires documenting and solidifying
best practice.

### Core Migrate API

At a high level, the core Migrate module (and possibly some related
contrib-space helpers) look like the best way to handle both the initial
creation of "fixtures" or "structural" content, as well as updating and
evolving it over time. More details to come, but this project is already
using this extensively to that end.

### Structure Sync

To address another portion of the "structural content" component, the
promising [Structure Sync](https://www.drupal.org/project/structure_sync)
module looks like a good addition to the toolchain/best practice here, as it
covers some common cases (taxonomies, blocks, and menus) where this kind of
"structure" is represented by content entities in Drupal 8.

Structure Sync takes the approach of generating its own config entity within
which to store the selected content items, and then exports them within that
config entity as part of the regular CMI export/import process.

I will spend some time exploring this module, and try to answer some of the
questions that arose in our initial assessment:

* test menu links and blocks as well as taxonomy terms (which Colan is already using)
* can we address "the ID problem?" namely, the exported config contains
  internal IDs that are not stable across site instances, and therefore prone
  to breakage where Drupal architecture relies on them.
  * in some cases we can work around these architecturally, but it will
    probably also be useful to have a resolution to [upstream issue #3032480](https://www.drupal.org/project/structure_sync/issues/3032480),
    and related [core issue #2761157](https://www.drupal.org/project/drupal/issues/2761157).

Related question:
* does `update_helper` have an alter hook mechanism where we can tweak what happens in the `hook_update()`
  * this would potentially allow us to automate the programmatic inclusion of "structure sync" contents and/or migrate module imports in a unified way.

Final note: this module is still fairly new, and a quick review of [the issue
queue](https://www.drupal.org/project/issues/structure_sync?categories=All)
indicates there may be some work to do to make this useful to us broadly.

## Code or DB updates

These are the most well-understood types of changes. Changes at a code level
will generally just take effect upon releasing the new code. However, a developer
primarily needs to consider how the new code may affect data or config stored
by the affected code, and provide an upgrade path via `hook_update()` or
similar, to ensure that:

* Database schema are updated
* Existing data is transformed appropriately
* Related config or state information is updated to reflect the change.
