---
title: Deployment
weight: 10

---

Deployment options:
{{% children %}}

System Requirements
-------------------

Refer to the general Drupal [system requirements](https://www.drupal.org/docs/user_guide/en/install-requirements.html).

### Server configuration

#### Database server

See the upstream [database server](https://www.drupal.org/docs/8/system-requirements/database-server) documentation.

#### Prerequisite Packages

Three categories of packages are needed when starting from a stock Ubuntu LTS
(18.04) image. Ensure the packages listed in the following scripts, or their
equivalents, are installed before running the [production (Jenkins) deployment
scripts](/deployment/jenkins):

* Apt and associated packages, installed by the [`apt.sh`](https://gitlab.com/consensus.enterprises/drumkit/-/blob/master/scripts/packer/scripts/apt.sh) script.
* System utility packages, installed by the [`utils.sh`](https://gitlab.com/consensus.enterprises/drumkit/-/blob/master/scripts/packer/scripts/utils.sh) script.
* PHP and related packages, installed by the [`php.sh`](https://gitlab.com/consensus.enterprises/drumkit/-/blob/master/scripts/packer/scripts/php.sh) script.

#### Web server

See the upstream [web server](https://www.drupal.org/docs/8/system-requirements/web-server#s-nginx) documentation.

The documentation for NGINX provides is a good starting point for [vhost configuration](https://www.nginx.com/resources/wiki/start/topics/recipes/drupal/).

Also, see the upstream [file and folder permissions](https://www.drupal.org/docs/8/system-requirements/web-server#s-file-and-folder-permissions) documentation.

#### PHP requirements

See the upstream [PHP requirements](https://www.drupal.org/docs/8/system-requirements/php-requirements), [extensions](https://www.drupal.org/docs/8/system-requirements/php-requirements#extensions) and [configuration](https://www.drupal.org/docs/8/system-requirements/php-requirements#twig) documentations.
