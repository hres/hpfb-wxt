---
title: Deployment on Aegir

---

Review the general [system requirements](../#system-requirements) before proceeding.

We use [Aegir](https://www.aegirproject.org) for most of our Drupal hosting, including QA sites. So a fair amount of what follows will assume familiarity with Aegir concepts and operations. In particular, it may be worthwhile to review the Aegir documentation for [Setting up a Platform](https://docs.aegirproject.org/usage/platforms/).

### Deploy an Updated Codebase

Once you've successfully [updated the codebase](/development/#updating-the-codebase), to deploy it for user-acceptance testing, create a platform using the following settings:

* **Name**: `HPFB WxT <RELEASE-TAG>`  (e.g., "HPFB WxT 0.2.0")
* **Deployment strategy**: Deploy a Composer project from a Git repository
* **Docroot**: `web`
* **Git URL**: `https://gitlab.com/hres/hpfb-wxt.git`
* **Branch/tag**: `<RELEASE-TAG>`  (e.g., "0.2.0")

Automating aspects of this are [being planned](https://gitlab.com/consensus.enterprises/saas/reqs/reqs/issues/12).

### Test the New Codebase

While we strive for complete test coverage in our [Continuous Integration](https://gitlab.com/hres/hpfb-wxt/pipelines), it's worthwhile to ensure that live sites with real data will update cleanly. The easiest way to do this is to clone some sites from the current platform to the new one, and manually check that everything continues to work as expected.

Automating aspects of this are also [being planned](https://gitlab.com/consensus.enterprises/saas/reqs/reqs/issues/11).


### Upgrade Instances

Next, we need to upgrade the sites we have running on the older platform.

1. Visit old platform node
2. Click "Migrate"
3. Select the new platform as the target.
4. Monitor progress and take note of any migrations that failed.
    1. Review the task log for failed migrations
    2. Retry the migration.
    3. If required, debug until the migration succeeds.
