---
title: Deployment via Jenkins

---

Review the general [system requirements](../#system-requirements) before proceeding.

Scripts are provided to enable three distinct tasks from Jenkins:
1. [Site installation (install.sh)](#install-script-installsh)
1. [Site re-installation (reinstall.sh)](#re-install-script-reinstallsh)
1. [Codebase update (update.sh)](#update-script-updatesh)

These scripts are configured by setting [environment variables](#environment-variables).


Environment Variables
---------------------

**CODEBASE_PATH** (e.g., `/var/www/hpfb-wxt`)
: The filesystem path where the codebase should be deployed.

**DOCROOT** (e.g., `/var/www/hpfb-wxt/web`)
: The path to the root of the Drupal application itself.

**REPO_URL** (e.g., `https://ci-deployment-tests:Yrhv-jmvJgRzFiaWmfbb@gitlab.com/hres/hpfb-wxt.git`)
: The URL of the repository containing the site's codebase. **N.B.** Since the repository is private, a deployment token with read access must be used.

**COMMIT_REF** (e.g., `v0.4.0`, `master`)
: The Git tag or branch at which to deploy or update the codebase.

**SITE_NAME** (e.g., `HPFB WxT`)
: The initial title of the site.

**SITE_URL** (e.g., `hpfb-wxt.hres.ca`)
: The URL at which the site will be hosted.

**INSTALL_PROFILE** (e.g., `hpfb_wxt`)
: The short name of the Drupal instalation profile.

**DB_NAME** (e.g., `hpfb_wxt_db_name`)
: The name of the database to use. **N.B.** The installation script expects a [database to have been created](https://www.drupal.org/docs/8/install/step-3-create-a-database#create-a-database-using-mysqlmariadb-commands).

**DB_USER** (e.g., `hpfb_wxt_db_user`)
: The username to access the database. **N.B.** The installation script expects a [user to have been granted proper permissions](https://www.drupal.org/docs/8/install/step-3-create-a-database#create-a-database-using-mysqlmariadb-commands).

**DB_PASS** (e.g., `my_super_secret_password`)
: The password to access the database.

**DB_SERVER** (e.g., `db0.hres.ca`)
: The URL (or IP address) of the database server.

**ADMIN_USER** (e.g., `admin`)
: The name of the administrator account.

**ADMIN_EMAIL** (e.g., `admin@hres.ca`)
: The email for the administrator account.


Install Script (`install.sh`)
-----------------------------

This script is found in the codebase at [`scripts/production/install.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/production/install.sh)

It expects the following [environment variables](#environment-variables):
* ADMIN_EMAIL
* ADMIN_USER
* CODEBASE_PATH
* COMMIT_REF
* INSTALL_PROFILE
* SITE_NAME
* DB_NAME
* DB_PASS
* DB_SERVER
* DB_USER
* REPO_URL
* SITE_URL

It performs the following tasks:
1. Clone the codebase from the specified Git repository to the desigated filesystem path.
1. Ensure that the codebase and any Git submodules are properly initialized, and checked-out at the correct commit. See: [`scripts/deploy/update-codebase.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/update-codebase.sh)
1. Install and run Composer to download dependencies. See: [`scripts/deploy/build-dependencies.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/build-dependencies.sh)
1. Install the site and run any required post-install tasks. See: [`scripts/deploy/install-site.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/install-site.sh)


Re-install Script (`reinstall.sh`)
----------------------------------

This script is found in the codebase at [`scripts/production/reinstall.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/production/reinstall.sh)

It expects the same [environment variables](#environment-variables) as the [installation script (install.sh)](#install-script-installsh).

It performs the following tasks:
1. Delete the existing site. See: [`scripts/deploy/delete-site.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/delete-site.sh)
1. Install the site and run any required post-install tasks. See: [`scripts/deploy/install-site.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/install-site.sh)


Update Script (`update.sh`)
---------------------------

This script is found in the codebase at [`scripts/production/update.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/production/update.sh)

It expects the following [environment variables](#environment-variables):
* CODEBASE_PATH
* SITE_URL
* COMMIT_REF

It performs the following tasks:
1. Put the site in maintenance mode. See: [`scripts/deploy/pre-update.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/pre-update.sh)
1. Ensure that the codebase and any Git submodules are updated, and checked-out at the correct commit. See: [`scripts/deploy/update-codebase.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/update-codebase.sh)
1. Run Composer to update dependencies. See: [`scripts/deploy/build-dependencies.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/build-dependencies.sh)
1. Run Drupal update functions. See: [`scripts/deploy/update-site.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/update-site.sh)
1. Take the site out of maintenance mode. See: [`scripts/deploy/post-update.sh`](https://gitlab.com/hres/hpfb-wxt/-/blob/master/scripts/deploy/post-update.sh)

