---
title: Manual Deployment

---

Review the general [system requirements](../#system-requirements) before proceeding.

The HPFB WxT [distribution](https://gitlab.com/hres/hpfb-wxt) has been built as a Drupal [installation profile](https://www.drupal.org/docs/8/distributions/creating-distributions/how-to-write-a-drupal-8-installation-profile), and so should follow standard Drupal [deployment and installation](https://www.drupal.org/docs/8/install) best practices. These are outlined below, with links to upstream documentation, whenever appropriate.

### Platform deployment

1. Deploy the codebase:
    1. Clone this project's repository: `git clone git@gitlab.com:hres/hpfb-wxt.git /path/to/web/root`
    1. [Install dependencies](https://www.drupal.org/docs/8/install/step-2-install-dependencies-with-composer): `composer install --no-dev`
1. Create a database, user and grant necessary privileges: refer to [upstream documentation](https://www.drupal.org/docs/8/install/step-3-create-a-database#create-a-database-using-mysqlmariadb-commands)


### Site installation

1. Run the Drupal install script to set up a new site. This should come up the first time you access the site. Refer to the [upstream documentation](https://www.drupal.org/docs/user_guide/en/install-run.html) for details.
    1. Be sure to select the "HPFB WxT" installation profile.
1. Check the status of the site and resolve any outstanding errors. Refer to the [upstream documentation](https://www.drupal.org/docs/8/install/step-6-status-check) for fixes to common issues.


### Post-installation tasks

1. [Create admin user account(s)](https://www.drupal.org/docs/user_guide/en/user-new-user.html):
    1. Username: `cgervais`, *email*: `christopher@consensus.enterprises`, *roles*: `Administrator`.
    1. Username: `dlaventure`, *email*: `derek@consensus.enterprises`, *roles*: `Administrator`.
    1. Username: `cschwartz`, *email*: `colan@consensus.enterprises`, *roles*: `Administrator`.

#### Additional tasks

Once provided with an administrator account, a HPFB WxT project team developer should then complete the following tasks:

1. Create additional user accounts
    1. Username: `tkim`, *email*: `taehyun.kim@canada.ca`, *roles*: `Power user`.
1. Set the default theme (`hpfb_wxt_bootstrap`)
1. Import data
    1. Enable `hpfb_wxt_live_data` module.
    1. Roll back partial Ingredients vocabulary import.
    1. Import Ingredients vocabulary (may require multiple runs).


Deploying Updates
-----------------

1. [Update core](https://www.drupal.org/docs/8/update/update-core-via-composer): TBD
1. [Update dependencies](https://www.drupal.org/docs/8/update/update-modules): TBD

