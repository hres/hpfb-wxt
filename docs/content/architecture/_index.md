---
title: Architecture
weight: 20

---

Handling Dependencies
---------------------

HPFB WxT is built on [Drupal](https://drupal.org) which makes it mostly PHP. Thus it uses [Composer](https://getcomposer.org) to manage dependencies between various modules, libraries, etc.


Information Architecture
------------------------

### Option Lists

The UI elements for several fields are drop-down select lists. The simplest way to implement these in Drupal is using the "Choice" module. However, once data for such a field exists, updating the available options becomes challenging. Instead, these have been implemented using Drupal's built-in Taxonomy system. This allows for terms to be added, modified or deleted as needed.

While this is a Drupal best practice, it presents a minor challenge: taxonomy terms are considered content, and thus are *not* exportable as configuration. We've thus implemented import mechanism (using the Migrate module), to seed these terms on installation. See the [Importing data](/architecture/importing_data) for implementation details.
