---
title: Example Wireframe
layout: wireframe

---

See: https://github.com/tsx/shireframe#tags

<a href="/">link</a>

<box>Hello there!|</box>

<row>
  <col-3 col-offset-1>First column</col-3>
  <col-3>Second column</col-3>
  <b col-3>Abusing B to make third column bold</b>
</row>

<fa star></fa>
<glyphicon user></glyphicon>

<kitten></kitten>

<cheerful-comment></cheerful-comment>
<angry-comment></angry-comment>

<lorem-ipsum></lorem-ipsum>

