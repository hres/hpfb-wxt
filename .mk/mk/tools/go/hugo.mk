hugo_NAME         ?= Hugo
hugo_RELEASE      ?= 0.73.0
hugo_DOWNLOAD_URL ?= https://github.com/gohugoio/hugo/releases/download/v$(hugo_RELEASE)/hugo_extended_$(hugo_RELEASE)_Linux-64bit.tar.gz

# vi:syntax=makefile
