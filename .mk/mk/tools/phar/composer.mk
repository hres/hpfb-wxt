composer_NAME         ?= Composer
composer_RELEASE      ?= 1.10.6
composer_DOWNLOAD_URL ?= https://getcomposer.org/download/$(composer_RELEASE)/composer.phar
composer_DEPENDENCIES ?= git php5-cli

# vi:syntax=makefile
