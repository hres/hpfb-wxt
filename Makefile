include .mk/GNUmakefile

INSTALL_PROFILE=hpfb_wxt

all: start build install

dev: install test-fixtures dev-enable snapshot
dev-enable: dev-en dev-users
dev-en:
	$(DRUSH) pm-enable -y hpfb_wxt_devel
dev-users:
	$(DRUSH) user:create test-manager --mail="test-manager@example.com" --password="pwd"
	$(DRUSH) user-add-role "hpfb_wxt_manager" test-manager
	$(DRUSH) user:create test-admin --mail="test-admin@example.com" --password="pwd"
	$(DRUSH) user-add-role "hpfb_wxt_administrator" test-admin

test-fixtures:
#	$(DRUSH) pm-enable -y hpfb_wxt_test_data
