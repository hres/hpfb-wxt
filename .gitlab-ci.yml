image:
  name: registry.gitlab.com/hres/hpfb-wxt/hpfb-wxt:latest
  entrypoint: [""] # We have to override the container entrypoint or else we end up in /bin/sh and `. d` doesn't work. See https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#overriding-the-entrypoint-of-an-image
  # TODO: Fix this entrypoint issue in the packer image.

# TODO: Move to using gitlab CI includes.

stages:
  - test
  - publish

variables:
  FF_NETWORK_PER_BUILD: 'true' # Allow service containers to see main build container; see https://docs.gitlab.com/runner/executors/docker.html#networking
  GIT_SUBMODULE_STRATEGY: recursive
  DEBIAN_FRONTEND: noninteractive
  HUGO_SITE_DIR: docs
  COMPOSER_CACHE_DIR: /tmp/composer-cache
  SITE_URL: build # Use 'build' alias for main job container.
  MYSQL_DATABASE: drupal8
  MYSQL_USER: drupal8
  MYSQL_PASSWORD: drupal8
  MYSQL_ROOT_PASSWORD: mysql_strong_password
  CODEBASE_PATH: /var/www/hpfb-wxt
  DOCROOT: "${CODEBASE_PATH}/web"

tests: &test_config
  stage: test
  services:
    - name: mysql:5.7 # TODO: move mysql server into our custom image to reduce launch time
      alias: database
    - name: selenium/standalone-chrome
      alias: chromedriver
  before_script:
    - scripts/ci/setup.sh
  script:
    #- tail -f /dev/null # localdev trick: keep the CI container around so we can look at it
    - scripts/ci/test.sh

deploy-install-tests: &deploy_tests
  <<: *test_config
  variables: &deploy_test_vars
    REPO_URL: "${CI_REPOSITORY_URL}"
    COMMIT_REF: "${CI_COMMIT_REF_NAME}"
    SITE_NAME: "HPFB Drupal WxT"
    SITE_URL: "build"
    INSTALL_PROFILE: "hpfb_wxt"
    DB_NAME: "${MYSQL_DATABASE}"
    DB_USER: "${MYSQL_USER}"
    DB_PASS: "${MYSQL_PASSWORD}"
    DB_SERVER: "database"
    ADMIN_USER: "admin"
    ADMIN_EMAIL: "${ADMIN_USER}@${SITE_URL}"
  script:
    - rm -rf ${CODEBASE_PATH}
    - scripts/deploy/deploy-codebase.sh
    - scripts/deploy/update-codebase.sh
    - "if [ ! -d \"${CODEBASE_PATH}\" ]; then echo 'Error: Clone failed.'; exit 1; fi"            # Check that code is checked out
    - "if [ -d \"${CODEBASE_PATH}/vendor\" ]; then echo 'Error: Vendor dir exists.'; exit 1; fi"  # Check that vendor dir does not exist.
    - scripts/deploy/build-dependencies.sh
    - "if [ ! -d \"${CODEBASE_PATH}/vendor\" ]; then echo 'Error: Composer failed.'; exit 1; fi"  # Check that vendor dir exists.
    - "if [ ! -d \"${DOCROOT}/core\" ]; then echo 'Error: Composer failed.'; exit 1; fi"          # Check that core was deployed.
    - scripts/deploy/install-site.sh
    - "if [ ! -d \"${DOCROOT}/sites/${SITE_URL}\" ]; then echo 'Error: Site installation failed.'; exit 1; fi"                       # Check that site dir was created.
    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} status | grep 'Drupal bootstrap' | grep 'Successful'"          # Check that site bootstraps.
    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} status | grep 'Install profile' | grep \"${INSTALL_PROFILE}\"" # Check that proper profile was used
    - scripts/deploy/delete-site.sh
    - "if [ -d \"${DOCROOT}/sites/${SITE_URL}\" ]; then echo 'Error: Site deletion failed.'; exit 1; fi"         # Check that site dir was deleted.
    - scripts/deploy/install-site.sh # Confirm delete-site still allows install-site to occur afterward.
    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} status | grep 'Drupal bootstrap' | grep 'Successful'"  # Check that site bootstraps.
    - scripts/ci/apache-setup.sh                              # Set up Apache vhost.

    - "cd ${CODEBASE_PATH}; bin/behat --profile=ci features/admin.feature" # Behat smoke test (login as an administrator)

deploy-update-tests:
  <<: *deploy_tests
  variables:
    <<: *deploy_test_vars
    PRE_UPDATE_COMMIT_REF: "${CI_COMMIT_REF_NAME}"
    UPDATE_COMMIT_REF: "${CI_COMMIT_REF_NAME}"
#TODO: reenable once we have an implementation of hook_update():
#    PRE_UPDATE_SCHEMA_VERSION: "8001"
#    UPDATE_SCHEMA_VERSION: "8001"
  script:
    - rm -rf ${CODEBASE_PATH}
    - "COMMIT_REF=\"${PRE_UPDATE_COMMIT_REF}\" scripts/deploy/deploy-codebase.sh"
    - "COMMIT_REF=\"${PRE_UPDATE_COMMIT_REF}\" scripts/deploy/update-codebase.sh"
    - scripts/deploy/build-dependencies.sh
    - scripts/deploy/install-site.sh
    - scripts/ci/apache-setup.sh                              # Set up Apache vhost.
    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} status | grep 'Drupal bootstrap' | grep 'Successful'"          # Check that site bootstraps.
    - "cd ${CODEBASE_PATH}; bin/behat --profile=ci features/admin.feature" # Behat smoke test (login as an administrator)
    - "cd ${CODEBASE_PATH}; bin/behat --profile=ci --tags=\"@maintenance-mode-disabled\" features/maintenance-mode.feature" # Check that site is not in maintenance mode.
    - "cd ${CI_PROJECT_DIR}"
    - scripts/deploy/pre-update.sh
    - "cd ${CODEBASE_PATH}; bin/behat --profile=ci --tags=\"@maintenance-mode-enabled\" features/maintenance-mode.feature" # Check that site is in maintenance mode.
    - "cd ${CODEBASE_PATH}; git rev-parse --abbrev-ref HEAD | grep \"${PRE_UPDATE_COMMIT_REF}\"" # Check that we're at correct tag (PRE_UPDATE_COMMIT_REF)
    - "cd ${CI_PROJECT_DIR}"
    - "COMMIT_REF=\"${UPDATE_COMMIT_REF}\" scripts/deploy/update-codebase.sh"
    - scripts/deploy/build-dependencies.sh
    - "cd ${CODEBASE_PATH}; git rev-parse --abbrev-ref HEAD | grep \"${UPDATE_COMMIT_REF}\"" # Check that we're at correct tag (UPDATE_COMMIT_REF)
#TODO: reenable once we have an implementation of hook_update():
#    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} php:eval \"print_r(drupal_get_installed_schema_version('hpfb_wxt_base'));\" | grep \"${PRE_UPDATE_SCHEMA_VERSION}\"" # Check that we're at correct schema version (PRE_UPDATE_SCHEMA_VERSION)
    - "cd ${CI_PROJECT_DIR}"
    - scripts/deploy/update-site.sh
#TODO: reenable once we have an implementation of hook_update():
#    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} php:eval \"print_r(drupal_get_installed_schema_version('hpfb_wxt_base'));\" | grep \"${UPDATE_SCHEMA_VERSION}\"" # Check that we're at correct schema version (UPDATE_SCHEMA_VERSION)
    - "cd ${CI_PROJECT_DIR}"
    - scripts/deploy/post-update.sh
    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} status | grep 'Drupal bootstrap' | grep 'Successful'"          # Check that site bootstraps.
    - "cd ${CODEBASE_PATH}; bin/behat --profile=ci features/admin.feature" # Behat smoke test (login as an administrator)
    - "cd ${CODEBASE_PATH}; bin/behat --profile=ci --tags=\"@maintenance-mode-disabled\" features/maintenance-mode.feature" # Check that site is not in maintenance mode.

production-deploy-tests:
  <<: *deploy_tests
  image:
    name: ubuntu:bionic # This is for production, so we use a stock Ubuntu.
    entrypoint: [""]    # See above.
  before_script:
    - scripts/packer/scripts/apt.sh
    - scripts/packer/scripts/utils.sh
    - scripts/packer/scripts/php.sh
  script:
    - rm -rf ${CODEBASE_PATH}
    - scripts/production/install.sh
    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} status | grep 'Drupal bootstrap' | grep 'Successful'"          # Check that site bootstraps.
    - scripts/production/reinstall.sh
    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} status | grep 'Drupal bootstrap' | grep 'Successful'"          # Check that site bootstraps.
    - scripts/production/update.sh
    - scripts/ci/apache-setup.sh                              # Set up Apache vhost.
    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} status | grep 'Drupal bootstrap' | grep 'Successful'"          # Check that site bootstraps.
    - "cd ${CODEBASE_PATH}; bin/behat --profile=ci features/admin.feature" # Behat smoke test (login as an administrator)
    - "cd ${CODEBASE_PATH}; bin/behat --profile=ci --tags=\"@maintenance-mode-disabled\" features/maintenance-mode.feature" # Check that site is not in maintenance mode.
# TODO: get this working somehow (--no-dev install will remove drush; see reverted commit 1b464de)
#    # Ensure config_enforce is enabled:
#    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} pm-list --type=module --no-core --status=enabled --filter=config_enforce|grep config_enforce"
#    # Ensure config_enforce_devel is not enabled:
#    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} pm-list --type=module --no-core --status=enabled --filter=config_enforce|grep -v config_enforce_devel"
#    # Ensure config_enforce_devel is not available on the platform:
#    - "${CODEBASE_PATH}/bin/drush --root=${DOCROOT} --uri=${SITE_URL} pm-list --type=module --no-core --filter=config_enforce_devel|grep -v 'Config Enforce - Devel'"

pages:
  stage: publish
  image: registry.gitlab.com/pages/hugo:latest
  before_script:
    # Check the current version of Hugo, so we can keep our local env in sync.
    - hugo version
  script:
    # Build our docs site.
    - cd $HUGO_SITE_DIR && hugo && mv public/ ..
  artifacts:
    paths:
      - public
  only:
    - master
  when: manual

