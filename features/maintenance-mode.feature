# Silence watchdog errors generated by wxt with @errors til we get them fixed upstream.
@maintenance-mode @errors
Feature: Maintenance mode.
  In order to deploy the application code
  As a site administrator
  I need to be able to put the site into maintenance mode.

  Background:
    Given I am not logged in

  @maintenance-mode-disabled
  Scenario: Site is not in maintenance mode.
    When I am on the homepage
    Then I should not see "is currently under maintenance. We should be back shortly. Thank you for your patience."

  @maintenance-mode-enabled
  Scenario: Site is in maintenance mode.
    When I am on the homepage
    Then I should see "is currently under maintenance. We should be back shortly. Thank you for your patience."
  
