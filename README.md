HPFB Drupal WxT: shared Drupal distribution
===========================================

This is a shared Drupal WxT distribution for HPFB.

## Getting Started

This codebase uses the [Lando](https://lando.dev) local development tool for
easy development workflows. This Docker-based tool is easy to install on all
platforms.

To instantiate a local development environment, take these steps:

```
. d
make start
make build
make install
```

From here, you can point your browser to http://hpfb-wxt.lndo.site to see a working
local instance of the site. You can log in to the localdev site with username
`dev` and password `pwd`.
