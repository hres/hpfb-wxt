{
    "name": "hc/hpfb-wxt",
    "description": "Shared Drupal WxT distribution for HPFB",
    "type": "project",
    "license": "GPL-2.0-or-later",
    "homepage": "https://gitlab.com/hres/hpfb-wxt",
    "support": {
        "docs": "https://gitlab.com/hres/hpfb-wxt/-/wikis/home"
    },
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "composer",
            "url": "https://asset-packagist.org"
        },
        {
            "type": "composer",
            "url": "https://drupalwxt.github.io/composer-extdeps/"
        }
    ],
    "replace": {
    },
    "require": {
        "acquia/lightning": "^4.1.1",
        "composer/installers": "^1.2",
        "cweagans/composer-patches": "~1.0",
        "drupal/admin_toolbar": "^2.0",
        "drupal/adminimal_admin_toolbar": "^1.10",
        "drupal/adminimal_theme": "^1.5",
        "drupal/bootstrap": "^3.20",
        "drupal/config_enforce": "^1.0-beta3",
        "drupal/config_profile": "^1.0",
        "drupal/config_update": "^1.6",
        "drupal/core": "^8.8",
        "drupal/core-composer-scaffold": "^8.8",
        "drupal/core-project-message": "^8.8",
        "drupal/core-recommended": "^8.8",
        "drupal/redirect_after_login": "^2.6",
        "drupal/simple_modal_entity_form": "1.x-dev",
        "drupal/tour_ui": "^1.0@beta",
        "drupal/views_field_permissions": "^2.0@alpha",
        "drupalwxt/wxt": "^3.0.6",
        "oomphinc/composer-installers-extender": "^1.1"
    },
    "require-dev": {
        "behat/behat": "^3.0",
        "behat/mink": "~1.7",
        "behat/mink-extension": "*",
        "behat/mink-goutte-driver": "*",
        "behat/mink-selenium2-driver": "*",
        "composer/composer": "^1.4",
        "drupal/config_enforce_devel": "^1.0-beta3",
        "drupal/devel": "*",
        "drupal/devel_debug_log": "*",
        "drush/drush": "^9.0",
        "drupal/console": "~1.9.0",
        "drupal/drupal-extension": "~3.4.0",
        "drupal/migrate_devel": "*",
        "jcalderonzumba/gastonjs": "~1.0.2",
        "jcalderonzumba/mink-phantomjs-driver": "~0.3.1",
        "mikey179/vfsstream": "~1.2",
        "pdepend/pdepend": "2.1.0",
        "phpmd/phpmd": "@stable",
        "phpunit/phpunit": "^4.8.35 || ^6.1",
        "sebastian/phpcpd": "*",
        "squizlabs/php_codesniffer": "2.*",
        "symfony/css-selector": "~2.8|~3.0",
        "symfony/phpunit-bridge": "^3.4.3"
    },
    "scripts": {
      "drupal-scaffold": "DrupalComposer\\DrupalScaffold\\Plugin::scaffold",
      "post-root-package-install": [
          "DrupalWxT\\WxT\\ScriptHandler::postCreateProject"
      ],
      "pre-install-cmd": [
          "DrupalWxT\\WxT\\ScriptHandler::checkComposerVersion"
      ],
      "pre-update-cmd": [
          "DrupalWxT\\WxT\\ScriptHandler::checkComposerVersion"
      ],
      "post-install-cmd": [
          "@composer drupal-scaffold",
          "DrupalWxT\\WxT\\ScriptHandler::deployLibraries"
      ],
      "post-update-cmd": [
          "@composer drupal-scaffold",
          "DrupalWxT\\WxT\\ScriptHandler::deployLibraries"
      ]
    },
    "conflict": {
        "drupal/drupal": "*"
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "config": {
        "bin-dir": "bin/",
        "prestissimo": {
            "maxConnections": 10,
            "minConnections": 5
        },
        "secure-http": false,
        "sort-packages": true,
        "discard-changes": true
    },
    "autoload": {
        "classmap": [
            "scripts/ScriptHandler.php"
        ]
    },
    "extra": {
        "drupal-scaffold": {
            "locations": {
                "web-root": "web/"
            }
        },
        "installer-types": [
           "bower-asset",
           "npm-asset"
        ],
        "installer-paths": {
            "drush/Commands/contrib/{$name}": [
                "type:drupal-drush"
            ],
            "web/core": [
                "type:drupal-core"
            ],
            "web/libraries/{$name}" : [
                "type:bower-asset",
                "type:drupal-console-library",
                "type:drupal-library",
                "type:npm-asset"
            ],
            "web/modules/contrib/{$name}": [
                "type:drupal-module"
            ],
            "web/modules/custom/{$name}": [
                "drupal/wxt_library",
                "type:drupal-custom-module"
            ],
            "web/profiles/contrib/{$name}": [
                "type:drupal-profile"
            ],
            "web/themes/contrib/{$name}": [
                "type:drupal-theme"
            ],
            "web/themes/custom/{$name}": [
                "drupal/wxt_bootstrap",
                "type:drupal-custom-theme"
            ]
        },
        "drupal-core-project-message": {
            "include-keys": [
                "homepage",
                "support"
            ],
            "post-create-project-cmd-message": [
                "<bg=blue;fg=white>                                                         </>",
                "<bg=blue;fg=white>  Congratulations, you\u2019ve installed the Drupal codebase  </>",
                "<bg=blue;fg=white>  from the drupal/recommended-project template!          </>",
                "<bg=blue;fg=white>                                                         </>",
                "",
                "<bg=yellow;fg=black>Next steps</>:",
                "  * Install the site: https://www.drupal.org/docs/8/install",
                "  * Read the user guide: https://www.drupal.org/docs/user_guide/en/index.html",
                "  * Get support: https://www.drupal.org/support",
                "  * Get involved with the Drupal community:",
                "      https://www.drupal.org/getting-involved",
                "  * Remove the plugin that prints this message:",
                "      composer remove drupal/core-project-message"
            ]
        },
        "composer-exit-on-patch-failure": true,
        "enable-patching": true,
        "patches": {
            "deployer/recipes": {
                "Enter deployer/recipes patch #80 description here":
                "https://patch-diff.githubusercontent.com/raw/deployphp/recipes/pull/80.patch"
            },
            "drupal/core": {
                "3011276 - Enable setting config translations":
                "https://www.drupal.org/files/issues/2019-08-20/drush_cset_for_other_than_default_collections-3011276-6.patch"
            },
            "drupal/field_validation": {
                "2969304 - Allow validation of titles":
                "https://www.drupal.org/files/issues/2019-04-11/field_validation-no_validation_on_title-2969304-4.patch"
            },
            "drupal/simple_modal_entity_form": {
                "3097564 - Destination parameter is not set":
                "https://www.drupal.org/files/issues/2019-11-29/simple_modal_entity_form-3097564-2.patch",
                "3121727 - Destination parameter incorrect on sub-directory installs":
                "https://www.drupal.org/files/issues/2020-04-06/simple_modal_entity_form-fix_destination_path-3121727-4.patch"
            },
            "drupal/migrate_tools": {
                "3099611 - Remove dependency on Drush command from UI":
                "https://www.drupal.org/files/issues/2019-12-11/3099611-15.patch",
                "3125378 - Fix MigrationGroup shared configuration breaks UI":
                "https://www.drupal.org/files/issues/2020-04-06/migrate_tools-fix_ui_for_shared_config-3125378-6.patch"
            },
            "drupal/views_data_export": {
                "3104129 - Allow filename to use tokens from overridden title based on contextual filters":
                "https://www.drupal.org/files/issues/2020-03-13/contextual-filename-fix_3104129_5.patch"
            },
            "drupalwxt/wxt": {
                "Make workflow/moderation configs optional.":
                "https://github.com/drupalwxt/wxt/commit/06dfbf0255fd770f3ad0a15bfb263115f2fc6de4.patch"
            }
        },
        "patches-ignore": {
            "drupal/lightning_core": {
                "drupal/core": {
                    "1356276 - Allow profiles to define a base/parent profile and load them in the correct order":
                    "https://www.drupal.org/files/issues/2019-11-05/1356276-531-8.8.x-4.patch",
                    "2914389 - Allow profiles to exclude dependencies of their parent":
                    "https://www.drupal.org/files/issues/2018-07-09/2914389-8-do-not-test.patch"
                }
            },
            "acquia/lightning": {
                "drupal/core": {
                    "3096566 - Copy media library styles from Seven to Claro & 3096241 - Refactor image and file field widgets to improve contrib compatibility and to make their templates and preprocess functions DRY":
                    "https://www.drupal.org/files/issues/2019-12-16/3096566-23-combined-3096241-3.patch"
                }
            },
            "drupalwxt/wxt": {
                "drupal/core": {
                    "2759397 - Patch EntityReferenceItemNormalizer to prevent recursion":
                    "https://www.drupal.org/files/issues/2759397-1-entity_reference_recursion.patch",
                    "Enter drupal/core patch #1356276 description here":
                    "https://www.drupal.org/files/issues/2019-12-27/1356276-88x-603.patch",
                    "Enter drupal/core patch #2914389 description here":
                    "https://www.drupal.org/files/issues/2019-10-22/2914389-14-do-not-test.patch"
                },
                "drupal/blog": {
                    "Enter drupal/blog patch #2834732 description here":
                    "https://www.drupal.org/files/issues/blog-no_results_text-2834732-2.patch"
                }
            }
        }
    }
}
