# Run our test suite.

.PHONY: tests tests-ci tests-wip tests-js vnc test-steps tests-upstream

tests-complete: tests tests-upstream
tests: ## Run Behat test suite
	$(BEHAT) --stop-on-failure

tests-upstream:
	$(BEHAT) --stop-on-failure --colors --suite=upstream --tags='~@wip'

tests-ci:
	$(BEHAT) --stop-on-failure --colors --profile=ci

tests-wip:
	$(BEHAT) --stop-on-failure --tags=wip

tests-js:
	$(BEHAT) --stop-on-failure --tags=javascript

test-steps:
	$(BEHAT) -dl
