#!/bin/bash

# Steps for setting up the Drupal site inside a CI docker image at packer time.

# Run a composer install to pre-populate its cache, which should speed up the process in CI.
# TODO: This location doesn't have to be app-specific, but has downstream dependencies to be dealt with. Make generic.
cd /var/www/hpfb-wxt
. d
make clean-build clean-composer-cache build VERBOSE=1
rm -rf /var/www/hpfb-wxt
