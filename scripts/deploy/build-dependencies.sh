#!/bin/bash -e

cd ${CODEBASE_PATH}
make composer
.mk/.local/bin/composer --ansi install --no-progress --no-interaction
