#!/bin/bash -e

cd ${CODEBASE_PATH}

# Take the site out of maintenance mode.
bin/drush --uri=${SITE_URL} state:set system.maintenance_mode 0 --input-format=integer
bin/drush --uri=${SITE_URL} cache:rebuild
