#!/bin/bash -e

cd ${CODEBASE_PATH}

git fetch
git checkout ${COMMIT_REF}
git submodule update --init --recursive
