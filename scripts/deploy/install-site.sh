#!/bin/bash -ev

cd ${CODEBASE_PATH}

# URL encode the password, in case it contains a slash (/).
export DB_PASS=$(php -r"print(urlencode('${DB_PASS}'));")

echo "bin/drush site:install ${INSTALL_PROFILE} -vvv --site-name=\"${SITE_NAME}\"  --yes --locale=\"en\" --db-url=\"mysql://${DB_USER}:${DB_PASS}@${DB_SERVER}/${DB_NAME}\" --sites-subdir=${SITE_URL} --account-name=\"${ADMIN_USER}\" --account-mail=\"${ADMIN_EMAIL}\""

bin/drush site:install ${INSTALL_PROFILE}\
                       -vvv --site-name="${SITE_NAME}" \
                       --yes --locale="en" \
                       --db-url="mysql://${DB_USER}:${DB_PASS}@${DB_SERVER}/${DB_NAME}" \
                       --sites-subdir=${SITE_URL} \
                       --account-name="${ADMIN_USER}" \
		       --account-mail="${ADMIN_EMAIL}"
