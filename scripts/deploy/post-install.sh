#!/bin/bash -e

# Finalize installation setup
bin/drush --uri=${SITE_URL} config-set -y system.theme default hpfb_wxt_bootstrap
bin/drush --uri=${SITE_URL} locale-check
bin/drush --uri=${SITE_URL} locale-update
bin/drush --uri=${SITE_URL} cr

# Create users
bin/drush --uri=${SITE_URL} user:create tkim --mail="taehyun.kim@canada.ca" --password="pwd"
bin/drush --uri=${SITE_URL} user-add-role "administrator" tkim
bin/drush --uri=${SITE_URL} user:create cgervais --mail="christopher@consensus.enterprises" --password="pwd"
bin/drush --uri=${SITE_URL} user-add-role "administrator" cgervais
bin/drush --uri=${SITE_URL} user:create dlaventure --mail="derek@consensus.enterprises" --password="pwd"
bin/drush --uri=${SITE_URL} user-add-role "administrator" dlaventure
bin/drush --uri=${SITE_URL} user:create cschwartz --mail="colan@consensus.enterprises" --password="pwd"
bin/drush --uri=${SITE_URL} user-add-role "administrator" cschwartz
