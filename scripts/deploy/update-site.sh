#!/bin/bash -e

cd ${CODEBASE_PATH}

bin/drush --uri=${SITE_URL} updatedb:status
bin/drush --uri=${SITE_URL} updatedb

