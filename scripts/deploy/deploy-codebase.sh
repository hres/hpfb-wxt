#!/bin/bash -e

echo "$0: About to run 'git clone --branch ${COMMIT_REF} ${REPO_URL} ${CODEBASE_PATH}'"

git clone --branch ${COMMIT_REF} ${REPO_URL} ${CODEBASE_PATH}
