#!/bin/bash -e

cd ${CODEBASE_PATH}

# Put the site in maintenance mode.
bin/drush --uri=${SITE_URL} state:set system.maintenance_mode 1 --input-format=integer
bin/drush --uri=${SITE_URL} cache:rebuild

