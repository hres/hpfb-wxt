#!/bin/bash -v

# Test script to be run in CI.

cd $CODEBASE_PATH
. d
make tests-ci
make -s matrix-ci NOTIFY_CI_RESULT=$?
