#!/bin/bash -e

# This script is useful for pausing an automated process (in a CI container,
# under development/troubleshooting, for example) until another process tells it to continue.

SLEEP=${1:-30}

# Sleep for $SLEEP seconds, watching for a file called "proceed" in /tmp
while [ ! -f /tmp/proceed ]
do
  echo "Sleeping for $SLEEP seconds..."
  sleep $SLEEP
done

echo "Proceeding."
rm -f /tmp/proceed
