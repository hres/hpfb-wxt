#!/bin/bash -e

# Make our docroot readable/executable by apache.
chown -R www-data $DOCROOT

# Deploy our vhosts.
cp scripts/ci/$SITE_URL.conf /etc/apache2/sites-available/$SITE_URL.conf
ln -sf /etc/apache2/sites-available/$SITE_URL.conf /etc/apache2/sites-enabled/$SITE_URL.conf

# Start the web server
service apache2 start
