#!/bin/bash -v

# Setup steps to be run inside CI docker image at CI time.
echo "Setting up site '$SITE_URL'."

# Update docroot with latest changes, clean up codebase.
ln -s `pwd` $CODEBASE_PATH
cd $CODEBASE_PATH

# Bootstrap Drumkit.
. d
# Build the site.
make composer
make build install test-fixtures

# Deploy our vhosts & start the web server.
scripts/ci/apache-setup.sh

# Make sure our base URL resolves.
echo `hostname -I` $SITE_URL >> /etc/hosts

# Check that a site has been installed.
[ -d web/sites/$SITE_URL ] || exit 1
./bin/drush --uri=$SITE_URL status
