#!/bin/bash -e

git clone --branch ${COMMIT_REF} ${REPO_URL} ${CODEBASE_PATH}
cd ${CODEBASE_PATH}
scripts/deploy/update-codebase.sh
scripts/deploy/build-dependencies.sh
scripts/deploy/install-site.sh
scripts/deploy/post-install.sh

