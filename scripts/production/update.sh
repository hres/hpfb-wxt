#!/bin/bash -e

cd ${CODEBASE_PATH}
scripts/deploy/pre-update.sh
scripts/deploy/update-codebase.sh
scripts/deploy/build-dependencies.sh
scripts/deploy/update-site.sh
scripts/deploy/post-update.sh

